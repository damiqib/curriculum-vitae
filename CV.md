# Janne Toikander

Experienced software developer and a system specialist.

## Personal Info

* [janne.toikander@gmail.com](mailto:janne.toikander@gmail.com)

## Currently

I'm currently working as a software developer at Barona Oy. Main buzzwords being Typescript, React, AWS, API's, Next.js.

## Work Experience

`05/2021 - present`
__Barona Oy__ Software Developer

`05/2021 - 05/2023`
__Caverion Industria Oy__ System Specialist Consultant (Part time)

`06/2020 - 05/2021`
__Caverion Industria Oy__ System Specialist

I was employed as a product owner / system specialist / integration architect in [Caverion Industria Oy](https://www.caverion.fi/). I was responsible of a work force management solution inherited from Maintpartner Oy and it's integration platform.

`11/2015 - 06/2020`
__Maintpartner Oy__ System Specialist

I was employed as a key user / system specialist (and after mid 2019 also as an integration architect) in Maintpartner Oy. I was responsible of a work force management solution and it's integration platform.

`11/2011 - 11/2015`
__Maintpartner Oy__ Team Leader

I worked as a small maintenance team's team leader for a food industry client.

`08/2010 - 11/2011`
__Maintpartner Oy__ Maintenance Technician

`04/2010 - 08/2010`
__VAASAN Oy__ Maintenance Electrician

`07/2008 - 04/2010`
__Eniweld Oy__ Instrument Technician

## Education

`2020 - present`
__XAMK__ Bachelor of Business Administration, Information Technology (_on-going_)

`2007 - 2008`
__FH Joanneum__ Global Business Program in Graz, Austria (_ERASMUS_)

`2005 - 2008`
__KyAMK (XAMK)__ Industrial Engineering and Management (_Never graduated_)

`2002 - 2005`
__EKAMI__ Electrician (_Graduated with excellent grades_)

## Technical Skills

* TypeScript
* React, Next.js
* JavaScript
* MeteorJS
* Node.js
* MySQL, MSSQL, Oracle
* MongoDB
* AWS
* PHP
* HTML
* CSS
* C# (basics)
* Kotlin (basics)

## Areas of Interest

* Photography
* Coding

## Other Activities

`2019 - 2022`

Four terms as a __chairperson__ of the [Kamera-73 Kuusankoski ry](http://www.kamera73.fi) (Kamera-73 Kuusankoski Photography Club)

## Languages

* Finnish (_native_)
* English (_proficient_)

## Links

* [janne.toikander@gmail.com](mailto:janne.toikander@gmail.com)
* [LinkedIn](https://www.linkedin.com/in/janne-toikander/)
* [GitLab](http://gitlab.com/damiqib)
* [GitHub](http://github.com/damiqib)
* [Instagram](https://www.instagram.com/jannetoikanderphotography/)
